<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wordpress' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'super' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'db' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Pcrr=-}i>JY/b`[3y675pP1L>jZ~@hY1L#)}7gi*E9dyZ4^W7_$-hCycB~6$|#|4' );
define( 'SECURE_AUTH_KEY',  '@> t>.bMAJ[HlBN$pcph*ayws[y7_.:J<Q=D6Tk9<:JWBJm!M6>N5uw<X1u2.&cB' );
define( 'LOGGED_IN_KEY',    'V{o9$y()>u.ZT5[*(**bI0&wlCA)TsZoTvmPOpUdM-68@Z<hx}~tA[n*j#]<4?5n' );
define( 'NONCE_KEY',        'CXBo7/.a4>X9Wgwh+t@siXF[%0)_^%i?EMrwXt?^+OOvnS0RcPa])h}:gGim+?Dq' );
define( 'AUTH_SALT',        'x$a;K$g^<J98hq``n>R!n$u?f6M}WAiM.(PUt}dI]` ?Hqt/Qkjhw_&DDTP&M5/?' );
define( 'SECURE_AUTH_SALT', 'U5key23G<ARQ1=czFPFUm{jT%HToH=e*G)<qX&Qg6?u&Nu4V_T kE_!UQ*hr)3d8' );
define( 'LOGGED_IN_SALT',   'aAY2*tV9Vw08a.5$.=usW>W/s<;iQ$<cI5:v}$yynfZi}+X*&NEC%=xFv$V0/~Q?' );
define( 'NONCE_SALT',       '^pL,E4FH0Lf3H.+/V%UNb6I-TO$VSoa*&L!>a}+%9gNsUCiH xCYTpcq4U_sxVji' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
